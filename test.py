import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

nltk.download('punkt')
nltk.download('stopwords')


with open('документ1.txt', 'r', encoding='utf-8') as file:
    doc1 = file.read()

with open('документ2.txt', 'r', encoding='utf-8') as file:
    doc2 = file.read()

def calculate_similarity(doc1, doc2):
    vectorizer = TfidfVectorizer()
    tfidf_matrix = vectorizer.fit_transform([doc1, doc2])
    cosine_sim = cosine_similarity(tfidf_matrix[0], tfidf_matrix[1])
    return cosine_sim[0][0]

similarity_score = calculate_similarity(doc1, doc2)
print("Косинусная мера схожести между документами: {:.2f}".format(similarity_score))
